echo "Remove ${HOME}/Library/Caches/CocoaPods"
rm -rf "${HOME}/Library/Caches/CocoaPods"

echo "cd ios/"
cd ios/

echo "Remove Pods/"
rm -rf Pods

echo "Remove build/"
rm -rf build

echo "Remove /Library/Developer/Xcode/DerivedData/"
rm -rf ~/Library/Developer/Xcode/DerivedData/*

echo "run pod install"
pod install

echo "cd back to project root directory/"
cd ..