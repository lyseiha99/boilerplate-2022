import { put, call, takeLatest } from 'redux-saga/effects';
import { alertError, errorMessage, loadingModal } from 'src/Utils';
import { REQUEST } from 'src/Libs';
import { 
  // {{pascalCase name}}: IMPORT CONSTANT HERE
  {{constantCase name}},
  {{camelCase name}}Actions, 
  } from './{{pascalCase name}}Action';

// ====================================== Functions ======================================

// {{pascalCase name}} GENERATORS GO HERE

function* handle{{pascalCase name}}Request({payload}) {
  try {
    yield call(loadingModal.show);
    yield put({{camelCase name}}Actions.on{{pascalCase name}}Success(payload));
    yield call(loadingModal.hide);
  } catch (error) {
    console.log('Handle{{pascalCase name}}RequestError: ', error);
    yield call(loadingModal.hide);
    yield put({{camelCase name}}Actions.on{{pascalCase name}}Failure());
    yield call(alertError, errorMessage(error));
  }
}

// ====================================== WATCHERS ======================================

// {{pascalCase name}} WATCHERS GO HERE
export function* watch{{pascalCase name}}Request() {
  yield takeLatest({{constantCase name}}[REQUEST], handle{{pascalCase name}}Request)
}
