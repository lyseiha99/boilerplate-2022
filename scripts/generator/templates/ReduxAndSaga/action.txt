import { createAction, createRequestTypes, REQUEST, SUCCESS, FAILURE } from 'src/Libs';

// ================ CONSTANTS ================

// {{pascalCase name}} CONSTANTS GO HERE
export const {{constantCase name}} = createRequestTypes('{{constantCase name}}');

// ================ ACTIONS ================

export const {{camelCase name}}Actions = {
  // {{pascalCase name}} ACTIONS GO HERE
  on{{pascalCase name}}Request: payload => createAction({{constantCase name}}[REQUEST], { payload }),
  on{{pascalCase name}}Success: payload => createAction({{constantCase name}}[SUCCESS], { payload }),
  on{{pascalCase name}}Failure: () => createAction({{constantCase name}}[FAILURE]),
};
