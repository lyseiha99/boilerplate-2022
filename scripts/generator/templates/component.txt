import React from 'react';
import { Text } from 'react-native';
import styled from 'styled-components/native';

const Styled = {
  Wrapper: styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
  `,
};

export function {{pascalCase name}}() {
  return (
    <Styled.Wrapper>
      <Text>
        {{titleCase name}}
      </Text>
    </Styled.Wrapper>
  );
}
