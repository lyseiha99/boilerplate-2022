
export function* watch{{pascalCase actionName}}Request() {
  yield takeLatest({{constantCase actionName}}[REQUEST], handle{{pascalCase actionName}}Request);
}
