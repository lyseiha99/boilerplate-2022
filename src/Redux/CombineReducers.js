import { combineReducers } from 'redux';
// PLOP: IMPORT REDUCER HERE
import { authReducer } from './Auth';

export const reducersCombined = combineReducers({
  // PLOP: COMBINE REDUCER HERE
  authReducer,
});
