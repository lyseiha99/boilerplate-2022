import { persistReducer, persistStore } from 'redux-persist';
import { compose, applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createLogger } from 'redux-logger';
import ImmutablePersistenceTransform from 'src/Services/ImmutablePersistenceTransform';
import Config from 'react-native-config';
import { reducersCombined } from './CombineReducers';
import { rootSaga } from './RootSaga';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

if (Config.ENV === 'DEV' || Config.ENV === 'QA') {
  const loggerMiddleware = createLogger();
  middlewares.push(loggerMiddleware);
}

const reducers = (state, action) => {
  // if (action.type === LOGOUT[SUCCESS]) {
  //   state = undefined;
  // }
  return reducersCombined(state, action);
};

const persistConfig = {
  key: 'primary',
  storage: AsyncStorage,
  whitelist: ['authReducer'],
  version: 0,
  transforms: [ImmutablePersistenceTransform],
  // migrate: createMigrate(migrations),
};

const enhancer = compose(applyMiddleware(...middlewares));
const persistedReducer = persistReducer(persistConfig, reducers);
const store = createStore(persistedReducer, enhancer);
const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export default store;
export { persistor };
