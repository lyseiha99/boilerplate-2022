import Immutable from 'seamless-immutable';
import { createSelector } from 'reselect';
import { createReducer, REQUEST, SUCCESS, FAILURE } from 'src/Libs';
import {
  // Auth ACTIONS GO HERE
  AUTH,
} from './AuthAction';

// ================ SELECTORS ================

// Auth SELECTORS GO HERE
const authReducerSelected = state => state.authReducer;
const isRequest = createSelector(authReducerSelected, authReducer => authReducer.isRequest);

export const authSelectors = {
  // Auth EXPORT SELECTORS GO HERE
  isRequest,
};

// ================ REDUCERS ================
const initialState = Immutable({
  isRequest: false,
});

export const authReducer = createReducer(initialState, {
  // Auth REDUCERS GO HERE
  [AUTH[REQUEST]]: state => {
    return state.merge({
      isRequest: true,
    });
  },
  [AUTH[SUCCESS]]: state => {
    return state.merge({
      isRequest: false,
    });
  },
  [AUTH[FAILURE]]: state => {
    return state.merge({
      isRequest: false,
    });
  },
});
