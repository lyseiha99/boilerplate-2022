import { put, call, takeLatest, delay } from 'redux-saga/effects';
import { alertError, errorMessage, loadingModal } from 'src/Utils';
import { REQUEST } from 'src/Libs';
import { NavigationAction } from 'src/Navigation/NavigationAction';
import {
  // Auth: IMPORT CONSTANT HERE
  AUTH,
  authActions,
} from './AuthAction';

// ====================================== Functions ======================================

// Auth GENERATORS GO HERE

function* handleAuthRequest() {
  try {
    yield call(loadingModal.show);
    yield delay(1000);
    yield call(loadingModal.hide);
    yield call(NavigationAction.reset, 'HomeScreen');
  } catch (error) {
    console.log('HandleAuthRequestError: ', error);
    // yield call(loadingModal.hide);
    yield put(authActions.onAuthFailure());
    yield call(alertError, errorMessage(error));
  }
}

// ====================================== WATCHERS ======================================

// Auth WATCHERS GO HERE
export function* watchAuthRequest() {
  yield takeLatest(AUTH[REQUEST], handleAuthRequest);
}
