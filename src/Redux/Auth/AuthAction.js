import { createAction, createRequestTypes, REQUEST, SUCCESS, FAILURE } from 'src/Libs';

// ================ CONSTANTS ================

// Auth CONSTANTS GO HERE
export const AUTH = createRequestTypes('AUTH');

// ================ ACTIONS ================

export const authActions = {
  // Auth ACTIONS GO HERE
  onAuthRequest: payload => createAction(AUTH[REQUEST], { payload }),
  onAuthSuccess: payload => createAction(AUTH[SUCCESS], { payload }),
  onAuthFailure: () => createAction(AUTH[FAILURE]),
};
