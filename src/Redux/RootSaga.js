import { all, fork } from 'redux-saga/effects';
// PLOP: IMPORT WATCHER HERE
import {
  // Auth: IMPORT WATCHER HERE
  watchAuthRequest,
} from './Auth';

export function* rootSaga() {
  yield all([
    // PLOP: FORK WATCHER HERE

    // Auth: FORK WATCHER HERE
    fork(watchAuthRequest),
  ]);
}
