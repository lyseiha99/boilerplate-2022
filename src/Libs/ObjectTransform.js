import _ from 'lodash';

const isObject = o => o === Object(o) && !isArray(o) && typeof o !== 'function';
const isArray = a => Array.isArray(a);

export const objectTransform = o => {
  if (isObject(o)) {
    const n = {};

    Object.keys(o).forEach(k => {
      n[_.camelCase(k)] = objectTransform(o[k]);
    });

    return n;
  }
  if (isArray(o)) {
    return o.map(i => {
      return objectTransform(i);
    });
  }

  return o;
};
