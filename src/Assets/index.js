import Annoyed from './Icons/annoyed.svg';
import NavBack from './Icons/nav-back.svg';
import Close from './Icons/close.svg';

export const IconAssets = {
  Annoyed,
  NavBack,
  Close,
};
