import apisauce from 'apisauce';
import Config from 'react-native-config';

let apiSauceInstance = null;

const initApi = (baseURL = Config.API_URL) => {
  apiSauceInstance = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
    },
    timeout: Config.TIMEOUT,
  });

  apiSauceInstance.addAsyncRequestTransform(request => async () => {
    console.log('Making Api Request: ', {
      url: request.url,
      payload: request.data,
      params: request.params,
    });
    return request;
  });

  apiSauceInstance.addAsyncResponseTransform(response => async () => {
    console.log(`Response /${response.config.url}`, response);
    if (!response.ok) throw response;
    return response;
  });
};

const api = {
  setHeaders: headers => apiSauceInstance.setHeaders(headers),
  setAuthtoken: token => {
    apiSauceInstance.setHeader('authorization', token);
  },
  deleteHeader: key => apiSauceInstance.deleteHeader(key),
};

export { api, initApi };
