import { Dimensions, StatusBar } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

const pixelScaler = size => RFValue(size);

const dimensions = Dimensions.get('window');

const { currentHeight } = StatusBar;
const { width, height } = dimensions;

const deviceHeight = StatusBar.currentHeight ? height - currentHeight : dimensions.height;

const device = {
  width,
  height: deviceHeight,
};

const metrics = {
  tiny: pixelScaler(3),
  small: pixelScaler(6),
  medium: pixelScaler(10),
  large: pixelScaler(16),
  MidLarge: pixelScaler(20),
  huge: pixelScaler(32),
  extraHuge: pixelScaler(62),
  borderRadius: pixelScaler(6),
  filterOptionBorderRadius: pixelScaler(16),
  defaultComponentSize: pixelScaler(40),
  image: {
    smallLogo: pixelScaler(32),
  },
};

const fontSize = {
  huge: pixelScaler(19),
  large: pixelScaler(16),
  regular: pixelScaler(14),
  medium: pixelScaler(12),
  small: pixelScaler(9),
};

export { metrics, device, pixelScaler, fontSize };
