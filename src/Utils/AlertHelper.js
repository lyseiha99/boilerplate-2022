/* eslint-disable default-param-last */
import { Alert } from 'react-native';
import i18n from 'i18next';

export function alertForceUpdate(
  message = i18n.t('message.defaultErrorMessage'),
  callback = () => {},
  laterPress = () => {},
  isForce,
) {
  Alert.alert(
    'New Version Available Update',
    message,
    isForce
      ? [
          {
            text: i18n.t('button.update'),
            onPress: callback,
          },
        ]
      : [
          {
            text: i18n.t('button.later'),
            onPress: laterPress,
          },
          {
            text: i18n.t('button.update'),
            onPress: callback,
          },
        ],
    { cancelable: false },
  );
}

export function alertError(
  message = i18n.t('message.defaultErrorMessage'),
  callback = () => {},
  title = i18n.t('message.defaultErrorTitle'),
  cancelable = false,
) {
  Alert.alert(
    title,
    message,
    [
      {
        text: i18n.t('button.ok'),
        onPress: () => {
          callback();
        },
      },
    ],
    { cancelable },
  );
}

export function prompt(message, title = i18n.t('message.defaultErrorTitle'), callback = () => {}, onCancel = () => {}) {
  Alert.alert(
    title,
    message,
    [
      {
        text: i18n.t('button.no'),
        style: 'cancel',
        onPress: () => onCancel(),
      },
      {
        text: i18n.t('button.yes'),
        onPress: () => callback(),
      },
    ],
    { cancelable: false },
  );
}

export function uploadImagePrompt(onLibrary = () => {}, onCamera = () => {}, onCancel = () => {}) {
  Alert.alert(
    i18n.t('message.uploadImagePromptTitle'),
    '',
    [
      {
        text: i18n.t('button.cancel'),
        style: 'cancel',
        onPress: onCancel,
      },
      {
        text: i18n.t('button.library'),
        onPress: () => onLibrary(),
      },
      {
        text: i18n.t('button.camera'),
        onPress: () => onCamera(),
      },
    ],
    { cancelable: true },
  );
}

export function inform(message, callback = () => {}, title = i18n.t('message.defaultInformTitle')) {
  Alert.alert(
    title,
    message,
    [
      {
        text: i18n.t('button.ok'),
        onPress: () => callback(),
      },
    ],
    { cancelable: false },
  );
}
