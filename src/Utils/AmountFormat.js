import numeral from 'numeral';

export function amountFormat(value, format = '0,0.00') {
  const amount = numeral(value).format(format);
  return `$ ${amount}`;
}

export function amountRielFormat(value) {
  const amount = numeral(value * 4100).format('0,00');
  return `${amount}៛`;
}
