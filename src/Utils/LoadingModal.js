import Progress from 'RNProgressHud';

export class loadingModal {
  static show() {
    console.log('LOADING SHOW');
    Progress.show();
  }

  static hide() {
    console.log('LOADING HIDE');
    Progress.dismiss();
  }
}
