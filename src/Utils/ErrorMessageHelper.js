import _ from 'lodash';
import i18n from 'i18next';

export const errorMessage = (error, defaultMessage = i18n.t('message.defaultErrorMessage')) => {
  const isString = _.isString(error);
  if (isString) return error;

  const errorMessageString = _.get(
    error,
    'data.errors[0]',
    _.get(error, 'code', _.get(error, 'data.message', error.message)),
  );

  if (_.isString(errorMessageString)) return errorMessageString;

  if (_.isArray(errorMessageString)) return errorMessageString[0];

  return defaultMessage;
};
