import moment from 'moment';

export const getDateAgo = date => {
  return moment(date).startOf('day').fromNow();
};

export function formatDate(date, format = 'DD MMM YYYY') {
  return moment(date).format(format);
}

export function formatDateForFilter(date) {
  return moment(date).format('YYYY-MM-DD');
}

export function formatDateTime(date, format = 'DD MMM YYYY hh:mma') {
  return moment(date).format(format);
}

export function convertToUTC(dateString) {
  return moment.utc(new Date(dateString)).local().format();
}

export function fixedDigit(number) {
  if (number.length === 1) {
    return `0${number}`;
  }
  return number;
}
