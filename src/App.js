import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { RootNavigationContainer } from './Navigation/RootNavigationContainer';
import store, { persistor } from './Redux/ConfigStore';
import { initApi } from './Services/Api';

const en = require('./Locales/en.json');
const km = require('./Locales/kh.json');

export default function App() {
  React.useEffect(() => {
    initApi();
    i18next.use(initReactI18next).init({
      compatibilityJSON: 'v3',
      resources: {
        en: {
          translation: en,
        },
        kh: {
          translation: km,
        },
      },
      lng: 'en',
      fallbackLng: 'en',
      interpolation: {
        escapeValue: false,
      },
    });
  }, []);

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootNavigationContainer />
        </PersistGate>
      </Provider>
    </GestureHandlerRootView>
  );
}
