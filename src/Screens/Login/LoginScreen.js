import React from 'react';
import { useDispatch } from 'react-redux';
import { PrimaryButton } from 'src/Components';
import { authActions } from 'src/Redux/Auth';
import { colors, metrics } from 'src/Themes';
import styled from 'styled-components/native';

const Styled = {
  Wrapper: styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    background-color: ${colors.white};
    padding: ${metrics.large}px;
  `,
};

export function LoginScreen() {
  const dispatch = useDispatch();

  const handleLoginPress = () => {
    dispatch(authActions.onAuthRequest());
  };

  return (
    <Styled.Wrapper>
      <PrimaryButton text="Login" onPress={handleLoginPress} />
    </Styled.Wrapper>
  );
}
