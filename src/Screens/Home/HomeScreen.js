import React from 'react';
import { RegularText } from 'src/Components';
import { colors, metrics } from 'src/Themes';
import styled from 'styled-components/native';

const Styled = {
  Wrapper: styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    background-color: ${colors.white};
    padding: ${metrics.large}px;
  `,
};

export function HomeScreen() {
  return (
    <Styled.Wrapper>
      <RegularText>Home</RegularText>
    </Styled.Wrapper>
  );
}
