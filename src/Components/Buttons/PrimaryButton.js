import React from 'react';
import { AppConfig } from 'src/Configs/AppConfig';
import { colors, metrics } from 'src/Themes';
import styled from 'styled-components/native';
import { RegularText } from '../StyledComponents';

// Styled components go here
const Styled = {
  Wrapper: styled.View.attrs(props => ({
    row: props.row,
  }))`
    width: ${props => (props.row === true ? 'auto' : '100%')};
    flex: ${props => (props.row ? 1 : 'none')};
  `,
  Button: styled.TouchableOpacity.attrs(props => ({
    width: props.width ? `${props.width}px` : '100%',
    backgroundColor: props.backgroundColor,
    activeOpacity: AppConfig.ACTIVE_OPACITY,
    marginHorizontal: props.marginHorizontal || 0,
  }))`
    height: 50px;
    width: ${props => props.width};
    padding-horizontal: ${metrics.large}px;
    background-color: ${props => props.backgroundColor};
    border-radius: ${metrics.borderRadius}px;
    justify-content: center;
    align-items: center;
    align-self: center;
    margin-horizontal: ${props => props.marginHorizontal}px;
  `,
};

export function PrimaryButton({ disabled, row, onPress, text, width, style, textStyle }) {
  return (
    <Styled.Wrapper row={row}>
      <Styled.Button
        backgroundColor={disabled ? colors.bg : 'black'}
        disabled={disabled}
        onPress={onPress}
        width={width}
        style={style}>
        <RegularText
          color={disabled ? colors.blueyGrey : colors.white}
          alignSelf="center"
          style={textStyle}
          fontWeight="bold">
          {text}
        </RegularText>
      </Styled.Button>
    </Styled.Wrapper>
  );
}
