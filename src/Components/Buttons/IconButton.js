import React from 'react';
import styled from 'styled-components/native';
import { TouchableOpacity } from 'react-native';
import { AppConfig } from 'src/Configs/AppConfig';

const Styled = {
  Wrapper: styled(TouchableOpacity)`
    width: 40px;
    height: 40px;
    align-items: center;
    justify-content: center;
  `,
};

export function IconButton({ disabled, icon, style = {}, onPress }) {
  return (
    <Styled.Wrapper disabled={disabled} style={style} activeOpacity={AppConfig.ACTIVE_OPACITY} onPress={onPress}>
      {icon}
    </Styled.Wrapper>
  );
}
