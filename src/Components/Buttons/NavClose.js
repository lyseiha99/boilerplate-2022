import React from 'react';
import { colors } from 'src/Themes';
import { IconAssets } from 'src/Assets';
import { NavigationAction } from 'src/Navigation/NavigationAction';
import { IconButton } from '.';

export function NavClose() {
  return (
    <IconButton
      style={{ backgroundColor: colors.white, borderRadius: 20, height: 32, width: 32 }}
      icon={<IconAssets.Close />}
      onPress={NavigationAction.back}
    />
  );
}
