import { colors } from 'src/Themes';
import styled from 'styled-components/native';

export const Divider = styled.View.attrs(props => ({
  column: props.column,
  height: props.height || 1,
  backgroundColor: props.backgroundColor || colors.bg,
}))`
  height: ${props => (props.column ? '100%' : `${props.height}px`)};
  width: ${props => (props.column ? '1px' : '100%')};
  background-color: ${props => props.backgroundColor};
`;
