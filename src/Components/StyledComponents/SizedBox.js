import { metrics } from 'src/Themes';
import styled from 'styled-components/native';

export const SizedBox = styled.View.attrs(props => ({
  height: props.height ? props.height : metrics.large,
  width: props.width ? props.width : metrics.large,
}))`
  height: ${props => props.height}px;
  width: ${props => props.width}px;
`;
