import { fontSize } from 'src/Themes';
import styled from 'styled-components/native';

export const RegularText = styled.Text.attrs(props => ({
  fontWeight: props.fontWeight,
  color: props.color || 'black',
  fontSize: props.fontSize || fontSize.regular,
  textAlign: props.textAlign || 'left',
}))`
  font-size: ${props => props.fontSize}px;
  font-weight: ${props => (props.fontWeight ? props.fontWeight : 'normal')};
  color: ${props => props.color};
  text-align: ${props => props.textAlign};
`;
