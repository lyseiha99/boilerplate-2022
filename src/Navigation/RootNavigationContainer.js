import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HomeScreen } from 'src/Screens';
import { StatusBar } from 'react-native';
import { NavBack } from 'src/Components';
import { navigationRef } from './NavigationAction';
import { AuthStack } from './Stack/AuthStack';

const Stack = createNativeStackNavigator();

export function RootNavigationContainer() {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator
          screenOptions={{
            headerBackTitle: '',
            headerLeft: NavBack,
            headerTitleAlign: 'center',
          }}
          mode="modal"
          initialRouteName="TabNavigation">
          <Stack.Screen
            options={{
              headerShown: false,
            }}
            name="AuthStack"
            component={AuthStack}
          />
          <Stack.Screen
            options={{
              headerShown: false,
            }}
            name="HomeScreen"
            component={HomeScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}
