import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { LoginScreen } from 'src/Screens';
import { NavBack } from 'src/Components';

const Stack = createNativeStackNavigator();

export function AuthStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerLeft: NavBack,
        headerTitleAlign: 'center',
      }}>
      <Stack.Screen
        options={{
          title: '',
          headerShown: false,
        }}
        name="LoginScreen"
        component={LoginScreen}
      />
    </Stack.Navigator>
  );
}
